# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Locality',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('abbreviations', models.CharField(max_length=3, blank=True, null=True)),
                ('parent_id', models.IntegerField(blank=True, null=True)),
                ('number', models.IntegerField(blank=True, null=True)),
                ('locality_type', models.CharField(max_length=24, blank=True, null=True)),
            ],
            options={
                'db_table': 'locality',
            },
        ),
    ]
