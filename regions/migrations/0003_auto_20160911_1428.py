# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('regions', '0002_auto_20160911_1427'),
    ]

    operations = [
        migrations.RenameField(
            model_name='locality',
            old_name='localita_type',
            new_name='locality_type',
        ),
    ]
