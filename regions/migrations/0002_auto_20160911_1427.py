# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('regions', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='locality',
            old_name='locality_type',
            new_name='localita_type',
        ),
    ]
