from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import user_passes_test
from django.views import generic
from django.core.urlresolvers import reverse_lazy

from .models import Locality


# This class uses to show list of locality
class LocalityView(generic.ListView):
    template_name = 'regions/index.html'
    context_object_name = 'regions_list'

    def get_queryset(self, **kwargs):
        if self.kwargs:
            return Locality.objects.filter(parent_id=self.kwargs['pk'])
        else:
            return Locality.objects.filter(parent_id=None)


# This class uses to create new locality
class LocalityCreate(generic.CreateView):
    model = Locality
    success_url = reverse_lazy('regions:index')
    fields = ['title', 'abbreviations', 'parent_id', 'number', 'locality_type']

    # This decorator and functionto give permission only for admin
    @method_decorator(user_passes_test(lambda u: u.is_superuser))
    def dispatch(self, *args, **kwargs):
        return super(LocalityCreate, self).dispatch(*args, **kwargs)


# This class uses to change locality
class LocalityUpdate(generic.UpdateView):
    model = Locality
    template_name_suffix = '_form'
    success_url = reverse_lazy('regions:index')
    fields = ['title', 'abbreviations', 'parent_id', 'number', 'locality_type']
    
    # This decorator and functionto give permission only for admin
    @method_decorator(user_passes_test(lambda u: u.is_superuser))
    def dispatch(self, *args, **kwargs):
        return super(LocalityUpdate, self).dispatch(*args, **kwargs)


# This class uses to delete locality
class LocalityDelete(generic.DeleteView):
    model = Locality
    template_name = 'regions/locality_delete.html'
    success_url = reverse_lazy('regions:index')
    
    # This decorator and functionto give permission only for admin
    @method_decorator(user_passes_test(lambda u: u.is_superuser))
    def dispatch(self, *args, **kwargs):
        return super(LocalityDelete, self).dispatch(*args, **kwargs)
