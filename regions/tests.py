from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

from .views import LocalityView


# This class checks view_class what  displays a list of localities
class LocalityViewTestCase(TestCase):
    ''''''
    def test_locality_view(self):
        response = self.client.get(reverse("regions:index"))
        self.assertEqual(response.status_code, 200)


# This class checks permissions for different users
class PermissionsTestCase(TestCase):

    # This function checks permission for Admin
    def test_permissions_for_superuser(self):
        self.user = User.objects.create_user(username='superuser', password='pass')
        self.user.is_superuser = True
        self.user.save()
        self.client.login(username='superuser', password='pass')
        response = self.client.get(reverse("regions:locality_create"), follow=True)
        self.assertEqual(response.status_code, 200)

    # This function checks permission for a simple user
    def test_permissions_for_user(self):
        self.user = User.objects.create_user(username='user', password='pass')
        self.user.save()
        self.client.login(username='user', password='pass')
        response = self.client.get(reverse("regions:locality_create"), follow=True)
        self.assertEqual(response.status_code, 404)
