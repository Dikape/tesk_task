from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.LocalityView.as_view(), name='index'),
    url(r'^(?P<pk>[0-9]+)/$', views.LocalityView.as_view(), name='locality'),
    url(r'^create_new$', views.LocalityCreate.as_view(), name='locality_create'),
    url(r'^update/(?P<pk>[0-9]+)/$', views.LocalityUpdate.as_view(), name='locality_update'),
    url(r'^delete/(?P<pk>[0-9]+)/$', views.LocalityDelete.as_view(), name='locality_delete'),
]