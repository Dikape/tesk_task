# This is an auto-generated Django model module.

from django.db import models


# This class uses to create table in database and work with this table
class Locality(models.Model):

    title = models.CharField(max_length=255)
    abbreviations = models.CharField(max_length=3, blank=True, null=True)
    parent_id = models.IntegerField(blank=True, null=True)
    number = models.IntegerField(blank=True, null=True)
    locality_type = models.CharField(max_length=24, blank=True, null=True)

    # This class uses to change standard name to "locality"
    class Meta:
        db_table = 'locality'

    def __str__(self):
        return self.title

